function Project({ projects }) {
  return (
    projects.map((project) => 
      <a
        href={project.link}
        key={project.name}
        className="sm:w-1/2 w-100 p-4">
        <div className="flex relative h-full">
          <img 
            alt="gallery"
            className="absolute inset-0 w-full h-full object-cover object-center bg-white rounded"
            src={project.images[0]?.resolutions.desktop.url ?? window.location + '/image-not-found.jpg'}
          />
          <div className="px-8 py-10 relative z-10 w-full border-4 border-gray-800 bg-gray-900 opacity-0 hover:opacity-100">
            <h2 className="tracking-widest text-sm title-font font-medium text-green-400 mb-1">
              {project.subtitle}
            </h2>
            <h1 className="title-font text-lg font-medium text-white mb-3">
              {project.displayName}
            </h1>
            <p className="leading-relaxed">{project.summary}</p>
          </div>
        </div>
      </a>
    )
  )
}

export default function Projects({projects}) {
    return (
      <section id="projects" className="text-gray-400 body-font">
        <div className="container px-5 py-10 mx-auto text-center lg:px-40">
          <div className="flex flex-col w-full mb-20">
            <h1 className="sm:text-4xl text-3xl font-medium title-font mb-4 text-white">
              Apps I've Built
            </h1>
            <p className="lg:w-2/3 mx-auto leading-relaxed text-base">
              Well, below are some of the applications that I developed in the past. Hope yours is next!
            </p>
          </div>
          <div className="flex flex-wrap -m-4">
            <Project projects={projects} />
          </div>
        </div>
      </section>
    )
}