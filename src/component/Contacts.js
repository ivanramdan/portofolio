export default function Contact() {
    return (
        <section id="contact" className="relative">
            <div className="container px-5 py-10 mx-auto flex sm:flex-nowrap flex-wrap" style={{height:'600px'}}>
                <div className="lg:w-full md:w-full bg-gray-900 rounded-lg overflow-hidden p-10 flex items-end justify-start relative">
                    <iframe
                        width="100%"
                        height="100%"
                        title="map"
                        className="absolute inset-0"
                        style={{ filter: "opacity(0.7)" }}
                        src="https://www.google.com/maps/embed/v1/place?q=241+peta+festival+citylink+bandung&key=AIzaSyBFw0Qbyq9zTFTd-tUY6dZWTgaQzuU17R8"
                    />
                    <div className="bg-gray-900 relative flex flex-wrap py-6 rounded shadow-md">
                        <div className="lg:w-1/2 px-6 mt-4 lg:mt-0">
                            <h2 className="title-font font-semibold text-white tracking-widest text-xs">
                                EMAIL
                            </h2>
                        <a href='google.com' className="text-indigo-400 leading-relaxed">
                            ivanramdan5@gmail.com
                        </a>
                        <h2 className="title-font font-semibold text-white tracking-widest text-xs mt-4">
                        PHONE
                        </h2>
                        <a href="https://wa.me/62895360806862">
                            <p className="text-indigo-400 leading-relaxed">+62895360806862</p>
                        </a>
                        </div>
                        <div className="lg:w-1/2 px-6">
                            <h2 className="title-font font-semibold text-white tracking-widest text-xs">
                                ADDRESS
                            </h2>
                            <p className="mt-1">
                                Bandung
                                <br />
                                Jawa Barat
                            </p>
                        </div>

                    </div>
                </div>
            </div>
        </section>
    );
  }