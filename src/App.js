import { useEffect, useState } from 'react'
import Navbar from './component/Navbar'
import About from './component/About'
import Projects from './component/Projects'
import Skills from './component/Skills'
import Testimonials from './component/Testimonials'
import Contacts from './component/Contacts'

export default function App() {
  const [user, setUser] = useState(null)

  useEffect(() => {
    fetch('https://gitconnected.com/v1/portfolio/ivanramdan')
      .then(res => res.json())
      .then(user => {
        setUser(user)
      })
  }, [])

  if(!user)
    return <div/>

  return (
    <main className="text-gray-400 bg-gray-900 body-font">
      <Navbar name={user.basics.name}/>
      <About about={user.basics}/>
      <Projects projects={user.projects}/>
      <Skills skills={user.skills}/>
      <Testimonials />
      <Contacts />
    </main>
  )
};
